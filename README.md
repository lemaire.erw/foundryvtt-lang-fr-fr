# FoundryVTT lang fr-FR

This module adds the option to select the French (FRANCE) language from the FoundryVTT settings menu. Selecting this option will translate various aspects of the program interface.

Installation
In the Add-On Modules option click on Install Module and place the following link in the field Manifest URL
https://gitlab.com/baktov.sugar/foundryvtt-lang-fr-fr/raw/master/fr-FR/module.json
If this option does not work download the fr-FR.zip file and extract its contents into the /resources/app/public/modules/ folder
Once this is done, enable the module in the settings of the world in which you intend to use it and then change the language in the settings.


Note : manifest for dnd5E translation : https://gitlab.com/baktov.sugar/foundryvtt-dnd5e-lang-fr-fr/raw/master/dnd5e_fr-FR/module.json


NOTE : Thx to Bellenus#5269 , Miriadis & Thomaz M. for copy his module : https://gitlab.com/elvis-pereira/foundryvtt-brazilian-portuguese